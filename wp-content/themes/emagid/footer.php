<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION STARTS -->
	<footer class="main_footer">

		<div class='footer_holder'>
            		 		<a href="/" class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/site_logo.png" alt="Adjuvant Health">
            </a>
			<div class="links">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-2'
                    ) );
                ?>
                
                <div class="social_links">
                    <ul>    
                        <li>
                            <a href="https://twitter.com/sociallyready" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png" alt="Twitter">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/teamsociallyready" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png" alt="Facebook">
                            </a>
                        </li>
                                                <li>
                            <a href="https://instagram.com/sociallyready" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig.png" alt="Instagram">
                            </a>
                        </li>
                                                <li>
                            <a href="https://youtube.com/" target="_blank">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png" alt="Youtube">
                            </a>
                        </li>
                        
                    </ul>
                </div>
			</div>
			
            
		</div>
	</footer>
	<!-- FOOTER SECTION ENDS -->



	

<?php wp_footer(); ?>

</body>
</html>
