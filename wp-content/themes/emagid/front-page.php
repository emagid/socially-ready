<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
                <?php the_field('hero_subtitle'); ?>
                <a href='/about' class='button'> Learn More </a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro'>
        <div class="container">
            <div>
                <?php the_field('portfolio_section_text'); ?>
            </div>

            <div class="container_overflow">
                <div class="row_3">
                    <a href="/services/parents-kids/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Parents & Kids</span></h3>
                    </a>
                </div>
                
                <div class="row_3">
                    <a href="/services/sports-entities/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Sports Entities</span></h3>
                    </a>
                </div>
                
                <div class="row_3">
                    <a href="/services/educational-institutions/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Educational Institutions</span></h3>
                    </a>
                </div>
                
                <div class="row_3">
                    <a href="/services/public-sectors/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Public Sectors</span></h3>
                    </a>
                </div>
                
                <div class="row_3">
                    <a href="/services/fraternal-organizations/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Fraternal Orgs</span></h3>
                    </a>
                </div>
                
                <div class="row_3">
                    <a href="/services/business-industry/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/comments.png">
                        <h3><span class="font_oj">Business Industry</span></h3>
                    </a>
                </div>
            </div>
            
            <div style="text-align:center;">
                <iframe src="https://player.vimeo.com/video/299908576" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
	</section>
	<!-- INTRO SECTION END -->


<!--TESTIMONIALS SECTION-->

<section class="testimonials">
    <div class="wrapper">
       <!-- Slider main container -->
<div class="swiper-container swiper1">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides --> 
                            <?php
	  			$args = array(
	    		'post_type' => 'testimonials',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        
        <div class="testimonial swiper-slide">
            <img src="<?php the_field('headshot'); ?>">
            <div class="content">
                <h5><?php the_field('testimonial'); ?></h5>
                <p><?php the_field('name'); ?></p>
            </div>
        </div>
        
                <?php
			}
				}
			else {
			echo 'No Testimonials Found';
			}
		?>   
           </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination swiper-pagination1"></div>

</div> 
    </div>
</section>

<!--END TESTIMONIALS SECTION-->

<!--CLIENTS SECTION-->
<!-- Slider main container -->
<div class="swiper-container swiper2">
    <h3>Our Clients</h3>
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
                    <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        <div class="logos swiper-slide">
            <img src="<?php the_field('logo'); ?>">
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination swiper-pagination2"></div>

    <!-- If we need navigation buttons -->
<!--
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
-->

    <!-- If we need scrollbar -->
    <div class="swiper-scrollbar"></div>
</div>





  <script>
  var swiper2 = new Swiper ('.swiper2', {
        autoplay: {
    delay: 2500,
  },
    // Optional parameters
    loop: true,
      slidesPerView: 6,
      spaceBetween: 40,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination2',
    },
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 40,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
  });

  </script>

<script>
      
        var swiper1 = new Swiper ('.swiper1', {
        autoplay: {
    delay: 5000,
  },
    // Optional parameters
//    loop: true,
      slidesPerView: 1,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination1',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
</script>

<?php
get_footer();
