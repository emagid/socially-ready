<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Inner Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('title'); ?></h1>
                <a href='' class='button'> Read More </a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro initiative'>
        <div class="container">
            <h2><?php the_field('header'); ?></h2>
                    <?php if (get_field('request_demo') != ''): ?>
        <p style="display:none;"><?php the_field('request_demo'); ?></p>
            <a href="#" class="request_demo"><button>Request A Demo</button></a>
        <?php endif; ?>
        
<!-- Calendly inline widget begin --><div class="demo_reveal" style="display:none;s">
<div class="calendly-inline-widget" data-url="https://calendly.com/carrie-sm2" style="min-width:320px;height:600px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script></div>
<!-- Calendly inline widget end -->
            <div>
                <?php the_field('content'); ?>
            </div>
        </div>


        
        
	</section>
	<!-- INTRO SECTION END -->



  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".intro").offset().top},
                'slow');
        });
      
      $(".request_demo").click(function(e){
          e.preventDefault();
    $(".demo_reveal").toggle();
});
  </script>

<?php
get_footer();
