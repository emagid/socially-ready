<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Institution Template
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('title'); ?></h1>
                <p><?php the_field('subtitle'); ?></p>
                <a href='' class='button'> View Courses </a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro pricing'>
        <div class="container">
            <h2><?php the_field('header'); ?></h2>
                    <?php if (get_field('request_demo') != ''): ?>
        <p style="display:none;"><?php the_field('request_demo'); ?></p>
            <a href="#" class="request_demo"><button>Request A Demo</button></a>
        <?php endif; ?>

            <div>
                <?php the_field('content'); ?>
            </div>
        </div>


        
        
	</section>
	<!-- INTRO SECTION END -->

<!--CLIENTS SECTION-->
<!-- Slider main container -->
<div class="swiper-container swiper2">
    <h3>COURSES TAKEN BY:</h3>
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
                    <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        <div class="logos swiper-slide">
            <img src="<?php the_field('logo'); ?>">
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination swiper-pagination2"></div>

    <!-- If we need navigation buttons -->
<!--
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
-->

    <!-- If we need scrollbar -->
<!--    <div class="swiper-scrollbar"></div>-->
</div>


<section>
    <div class="course_choices">
        <div class="container">
            
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'institutions', 'orderby' => 'rand' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
            
            
        <div class="course">
            <div class="wrapper">
                <?php the_title( '<h4>', '</h4>' ); ?>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>"><button>View Course</button></a>
            </div>
        </div>
            
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
            
    
    </div>
        
</div>
</section>




    <section class='bio contact_pricing'>
        <div class="contact_form">
            <h3>Contact For More Information</h3>
            <?php echo do_shortcode('[contact-form-7 id="30" title="Contact Form"]'); ?>
        </div>
	</section>

  <script>
      
              $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".course_choices").offset().top},
                'slow');
        });
      
  var swiper2 = new Swiper ('.swiper2', {
        autoplay: {
    delay: 2500,
  },
    // Optional parameters
    loop: true,
      slidesPerView: 6,
      spaceBetween: 40,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination2',
    },
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 40,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }
  });

  </script>

<?php
get_footer();
