<?php

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero blog_hero'>
        <div class="overlay">
            <div class='text_box'>
                <h1>My Account</h1>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->






<section>
    <div class="course_choices">
        <div class="container">
        <div class="course">
            <div class="wrapper">
                <h4>QUICK LINKS</h4>
                <p><a href="/account/orders">Recent Orders</a></p>
                <p><a href="/account/downloads">View Downloads</a></p>
                <p><a href="/account/edit-address">Manage Billing Address</a></p>
                <p><a href="/account/edit-account">Change Password</a></p>
                <p><a href="/account/edit-account">Edit Account Details</a></p>
            </div>
        </div>
            
        <div class="course">
            <div class="wrapper">
                <h4>Course 1</h4>
                <h5 style="text-align:left;">40% Complete</h5>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat v. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad olutpat. </p>
                <a href=""><button>Resume Course</button></a>
            </div>
        </div>

    <?php echo do_shortcode('[woocommerce_my_account]'); ?>
    </div>
        
</div>
</section>



  

<?php
get_footer();
