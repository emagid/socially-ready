<?php

get_header(); ?>

	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>Socially Ready Courses</h1>
                <p>Are you interested on behalf of an</p>
                <a href='/individual-courses' class='button'> Individual </a>
                <a href='/industry-courses' class='button'> Industry </a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->


<?php
get_footer();
