<?php

get_header(); ?>

	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>NEWS</h1>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->

<div class="news_wrapper">
    <?php echo do_shortcode('[display-posts image_size="medium" include_excerpt="true" include_date="true" excerpt_more="Read More" excerpt_more_link="true" posts_per_page="-1"]'); ?>
</div>

  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".icons").offset().top},
                'slow');
        });
  </script>
<?php
get_footer();